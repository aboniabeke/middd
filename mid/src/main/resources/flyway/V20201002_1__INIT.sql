CREATE TABLE person(
id serial primary key,
first_name VARCHAR(255),
last_name VARCHAR(255),
city VARCHAR(255),
phone VARCHAR(255),
telegram VARCHAR(255)
)